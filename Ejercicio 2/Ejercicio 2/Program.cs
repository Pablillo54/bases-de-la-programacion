﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Introduce un número: ");
            int numero = Convert.ToInt32(Console.ReadLine());

            int contador = 2;

            while (numero > 1)
            {
                if (numero % contador == 0)
                {
                    numero = numero / contador;
                    Console.Write("{0}", contador);
                }
                else contador++;
            }
            Console.ReadLine();
        }  
    }
}
