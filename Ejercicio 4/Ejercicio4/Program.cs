﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            int ancho, alto;

            Console.Write("Introduce el ancho del rectángulo: ");
            ancho = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introduce el alto del rectángulo: ");
            alto = Convert.ToInt32(Console.ReadLine());

            for (int y=1; y <= alto; y++)
            {
                for (int x = 1; x <= ancho; x++)
                {
                    Console.Write('*');
                }
            }
            Console.ReadLine();
        }
    }
}
