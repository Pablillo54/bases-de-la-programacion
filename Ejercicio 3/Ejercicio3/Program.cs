﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero, potencia;

            Console.Write("Introduce la base: ");
            numero = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introduce la potencia ");
            potencia = Convert.ToInt32(Console.ReadLine());

            int resultado = 1;

            while (potencia>0)
            {
                resultado = resultado * numero;
                potencia--;
            }
            Console.WriteLine("El resultado es {0}", resultado);
        }
    }
}
