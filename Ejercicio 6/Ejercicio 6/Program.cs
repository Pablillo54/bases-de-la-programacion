﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Introduce el ancho del rectángulo: ");
            int ancho = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introduce el alto del rectángulo: ");
            int alto = Convert.ToInt32(Console.ReadLine());

            for (int i=1; i <= alto; i++)
            {
                for(int j=1; j <= ancho; j++)
                {
                    if ((i == 1) || (i == alto) || (j == 1) || (j == ancho))
                    {
                        Console.Write("*");
                    } else Console.Write(' ');
                }
                Console.WriteLine();
            }
            Console.ReadLine();
           
        }
    }
}
