﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero, acertar = 36, i = 1;

            Console.WriteLine("Juguemos a intentar adivinar un número entre el 0 y el 11. Tenemos 6 intentos");

            while (i < 6)
            {
                Console.Write("\nIntroduce el {0}º número: ", i);
                numero = Convert.ToInt32(Console.ReadLine());
                if (numero > acertar)
                    Console.WriteLine("Has fallado. El número misterioso es menor...");
                if (numero < acertar)
                    Console.WriteLine("Has fallado. El número misterioso es mayor...");
                if (numero == acertar)
                {
                    Console.WriteLine("\n\n Has acertado. Genial!");
                    break;
                }
                if (i == 5)
                    Console.WriteLine("\n Cuidado, es tu última oportunidad");

                i++;
            }
        }
    }
}
