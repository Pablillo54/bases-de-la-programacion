﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int alto, altoModificado;

            Console.Write("Introduce el alto del rectángulo ");
            alto = Convert.ToInt32(Console.ReadLine());

            altoModificado = alto;

            for(int y=1; y <= alto; y++)
            {
                for(int x = 1; x <= altoModificado; x++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
                altoModificado--;
                Console.ReadLine();
            }
         } 
       
    }
}
